(function($) {
	'use strict';
	/**
	 * TextAreaValidator functions
	 *
	 * @class TextAreaValidator
	 */
	function TextAreaValidator() {
		/**
		 * @type {number}
		 * @private
		 */
		var maxChars = 4200;

		/**
		 * @type {number}
		 * @private
		 */
		var letterPerLineBreak = 90;

		/**
		 * @type {object}
		 * @private
		 */
		var $textarea = $('.maxcharcheck');

		/**
		 * @type {string}
		 * @private
		 */
		var chatCounterElementClassnames = 'fieldMessages fieldMessages__counter';


		/**
		 * @type {string}
		 * @private
		 */
		var chatCounterElementClassname = 'fieldMessages__counter';

		/**
		 * @type {string}
		 * @private
		 */
		var maxLengthClassname = 'fieldMessages__counter--error';

		/**
		 * initialized all functions
		 *
		 * @return {void}
		 */
		this.init = function() {
			addOutputContainerafterElement($textarea);
			$textarea.bind('input propertychange, keyup', function() {
				checkStringLength($(this));
			});

			$.each($textarea, function() {
				checkStringLength($(this));
			});
		};

		/**
		 * check is the added element the last element
		 *
		 * @returns {boolean}
		 */
		var checkLastElement = function(string, element) {
			var elementLength = element.length;
			if (string.substring(string.length - elementLength ) === element) {
				return true;
			}
			return false;
		};

		/**
		 * find \n in string and replace with |-|<br />
		 *
		 * @returns {Array}
		 */
		var replaceNewlineInContentAndSplit = function(string) {
			var replacedNewlineWithBreak = string.replace(/\r?\n/g, '|-|<br />');
			return replacedNewlineWithBreak.split('<br />');
		};

		/**
		 * return the calc string length
		 *
		 * @returns {int}
		 */
		var getStringLength = function(string) {
			if ('|-|' === string) {
				return letterPerLineBreak;
			} else {
				if (checkLastElement(string, '|-|')) {
					return string.length - 3;
				} else {
					return string.length;
				}
			}
		};

		/**
		 * check the length of the array element and shorten if is needed
		 *
		 * @returns {string}
		 */
		var shortenContent = function(contentArray) {
			var stringLength = 0;
			var newString = '';
			var singleLength = 0;
			var isStringShorten = false;
			$.each(contentArray, function(index, value) {
				singleLength = getStringLength(value);
				stringLength = (stringLength + singleLength);

				if (maxChars < stringLength) {
					var cutPosition = stringLength - maxChars;
					var shortenValue = value.substring(0, singleLength - cutPosition);
					newString += shortenValue.replace('|-|', '\n');
					isStringShorten = true;
					return false;
				}
				newString += value.replace('|-|', '\n');
			});
			return [isStringShorten, newString, stringLength];
		};

		var toLongTextAlert = function($outputcontainer, status) {
			if(status) {
				$outputcontainer.addClass(maxLengthClassname);
			} else {
				$outputcontainer.removeClass(maxLengthClassname);
			}
		};

		/**
		 * add div after element incl. max char text
		 *
		 * @returns {void}
		 */
		var addOutputContainerafterElement = function(element) {
			element.after($('<div />', {
				class: chatCounterElementClassnames,
				text: generateOutputText(maxChars)
			}));
		};

		/**
		 * returns max char text
		 *
		 * @returns {string}
		 */
		var generateOutputText = function(innerContent) {
			return 'noch ' + innerContent + ' Zeichen übrig';
			// return '(' + innerContent + ' characters remaining)';
		};

		/**
		 * initial main function
		 *
		 * @returns {void}
		 */
		var checkStringLength = function(textarea) {
			var content = textarea.val();
			var contentArray = replaceNewlineInContentAndSplit(content);
			var shortenStringArray = shortenContent(contentArray);
			var shortenStringIsShorten = shortenStringArray[0];
			var shortenString = shortenStringArray[1];
			var remainingChars = maxChars - shortenStringArray[2];
			var $outputcontainer = $('.' + chatCounterElementClassname);
			toLongTextAlert($outputcontainer, false);

			$outputcontainer[0].innerHTML = generateOutputText(remainingChars);
			if (shortenStringIsShorten) {
				textarea.val(shortenString);
				$outputcontainer[0].innerHTML = generateOutputText(0);
				toLongTextAlert($outputcontainer, true);
			}
		};
	}

	var textAreaValidator = new TextAreaValidator();
	textAreaValidator.init();
})(jQuery);
